# Base image.
FROM ubuntu


# Define working volumes
VOLUME ["/scripts/"]

# Set the DEBIAN_FRONTEND environment variable only during the build
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get install nano git curl ubuntu-desktop-minimal -y

RUN git clone https://gitlab.com/ikario404/mynicedesktop
COPY ./ /scripts/
WORKDIR /scripts/

# RUN groupadd -g 999 appuser && \
#     useradd -r -u 999 -g appuser appuser
# USER appuser

RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo

CMD sh /scripts/install.sh 