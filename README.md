# MyNiceDesktop

Bunch of scripts and config file for a nice dekstop shared between different computers with similar env and still tasting some unixporn.

***Requirements:***
- zsh
- ohmyzsh
- powerlevek10k
(optional -not for now but will be)
- i3
- polybar 
- polybar-scripts

## What's inside:
- config file for i3, polybar, polybar-scripts, zsh, also some alias and some scripts (bash or python)
- a minimal install script (meaning install dependancies, copy config file)
- a dockerfile for speed testing without messing his dekstop

### How to use
``` bash
	git clone git@gitlab.com:ikario404/mynicedesktop.git
	cd mynicedesktop
	sh ./install.sh
```

##### Optionnal :
Will need sudo and then install dependancies with apt-get
``` bash
	sh ./install.sh --install
```

#### To do
* list and document my config and plugins
* better install script

