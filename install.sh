#!/bin/sh
## git repo here :
## Version: 0.1
 
# chmod u+x ####
# sudo ./####

## dynamic entering path of install
## think to options or arguments

## logical order
# get $HOME path 
# verify if installed if yes pass next
# install dependancies packages
# .bak $HOME fileconf
# .bak also in specified current dir
# copy conf file in $HOME
# reload everything


## --Install part
if [ $1 ]; then
	if [ $1 = '-i' ] || [ $1 = '--install' ]; then
		echo 'install dependancies :'
		echo '------------------------'
		echo ''

		## Install dep
		apt-get update
		apt-get -y install i3 zsh 
		# ## Features: +alsa +curl +i3 +mpd +network(wireless-tools) +pulseaudio +xkeyboard

		# # Source for polybar compiling
		apt-get install build-essential git cmake cmake-data pkg-config python3-sphinx libcairo2-dev libxcb1-dev libxcb-util0-dev libxcb-randr0-dev libxcb-composite0-dev python3-xcbgen xcb-proto libxcb-image0-dev libxcb-ewmh-dev libxcb-icccm4-dev -y
		apt-get install libxcb-xkb-dev libxcb-xrm-dev libxcb-cursor-dev libasound2-dev libpulse-dev i3-wm libjsoncpp-dev libmpdclient-dev libcurl4-openssl-dev libnl-genl-3-dev -y
		git clone --recursive https://github.com/polybar/polybar
		mkdir $HOME/polybar/build
		cmake $HOME/polybar/build/..
		make -j$(nproc)
		make install

		# ## +fzf +Virtualenvwrapper +NVM_DIR +exa +you-should-use
		apt-get -y install ohmyzsh
		apt-get -y install powerlevel10k
		sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
		git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
	fi
fi

## Copy conf file
echo 'copy file config :'
echo '------------------------'
echo ''
cp -r ./font/ $HOME/.fonts
cp -r ./zsh/.zshrc $HOME/.zshrc
cp -r ./i3config/ $HOME/.config/i3/
cp -r ./i3status/ $HOME/.config/i3status/
cp -r ./polybarConfig/ ~/.config/polybarConfig/

